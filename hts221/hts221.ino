#include <Arduino_HTS221.h>

void setup() {

  Serial.begin(115200);
  while (!Serial);

  if (!HTS.begin()) {
    Serial.println("Failed to initialize humidity temperature sensor!");
    while (1);
  }
}

void loop() {
  // read all the sensor values
  float temperature = HTS.readTemperature();
  Serial.println(temperature);
  delay(10000);
}
